package com.authentication.utilities.service;

import com.authentication.models.entities.AccountLoginEntities;
import com.authentication.models.repos.AccountLoginRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailServiceImpl implements UserDetailsService{

    @Autowired
    AccountLoginRepository accountLoginRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AccountLoginEntities accountLoginEntities = accountLoginRepository.findByEmail(username)
            .orElseThrow(() -> new UsernameNotFoundException("Email " + username + " tidak ditemukan"));
        return UserDetailsImpl.build(accountLoginEntities);
    }
    
}
