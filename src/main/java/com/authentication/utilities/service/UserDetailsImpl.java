package com.authentication.utilities.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.authentication.models.entities.AccountLoginEntities;
import com.fasterxml.jackson.annotation.JsonIgnore;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;

import lombok.Data;

@Data
public class UserDetailsImpl implements UserDetails {
    
    private String fullName;
    private String email;
    @JsonIgnore
    private String password;

    public UserDetailsImpl(String fullName, String email, String password) {
        this.fullName = fullName;
        this.email = email;
        this.password = password;
    }

    public static UserDetailsImpl build(AccountLoginEntities accountLogin) {
        return new UserDetailsImpl(
            accountLogin.getFullName(), 
            accountLogin.getEmail(),
            accountLogin.getPassword());
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        if (StringUtils.hasText(fullName)) {
            String[] splits = fullName.replaceAll(" ", "").split(",");
            for (String string : splits) {
                authorities.add(new SimpleGrantedAuthority(string));
            }
        }
        return authorities;
    }

    @Override
    public String getUsername() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
