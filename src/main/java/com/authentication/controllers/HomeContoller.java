package com.authentication.controllers;

import com.authentication.config.JwtUtilsConfig;
import com.authentication.dto.RegisterDto;
import com.authentication.dto.ResponseData;
import com.authentication.models.entities.AccountProfileEntities;
import com.authentication.service.AccountProfileService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.validation.Valid;

@RestController
@RequestMapping("api/v1/main")
public class HomeContoller {

    @Autowired
    private JwtUtilsConfig jwtUtilsConfig;

    @Autowired
    private AccountProfileService accountProfileService;

    private ResponseData<AccountProfileEntities> responseData = new ResponseData<>();

    @GetMapping("/profile")
    public ResponseEntity<ResponseData<AccountProfileEntities>> profile(@RequestHeader("Authorization") String authorizationHeader) {
        String email = jwtUtilsConfig.getEmailFromJwtToken(authorizationHeader.replace("Bearer ", ""));
        responseData.setSuccess(true);
        responseData.setList(null);
        responseData.setData(accountProfileService.findUseEmail(email));
        return ResponseEntity.ok(responseData);
    }

    @GetMapping("/profile/edit")
    public ResponseEntity<ResponseData<AccountProfileEntities>> editProfile(@Valid @RequestBody RegisterDto regist, Errors errors) {
        return null;
    }
}
