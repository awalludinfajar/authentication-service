package com.authentication.controllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;

import com.authentication.config.JwtUtilsConfig;
import com.authentication.dto.AuthResponseDto;
import com.authentication.dto.LoginDto;
import com.authentication.dto.RefreshTokenDto;
import com.authentication.dto.RegisterDto;
import com.authentication.dto.ResponseData;
import com.authentication.models.entities.AccountLoginEntities;
import com.authentication.models.entities.AccountProfileEntities;
import com.authentication.service.AccountProfileService;
import com.authentication.utilities.service.UserDetailServiceImpl;
import com.authentication.utilities.service.UserDetailsImpl;

@RestController
@RequestMapping("api/v1/auth")
public class AuthController {

    private final JwtUtilsConfig jwtUtilsConfig;
    private final AuthenticationManager authenticationManager;

    public AuthController(JwtUtilsConfig jwtUtilsConfig, AuthenticationManager authenticationManager) {
        this.jwtUtilsConfig = jwtUtilsConfig;
        this.authenticationManager = authenticationManager;
    }

    @Autowired
    private AccountProfileService accountProfileService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserDetailServiceImpl userDetailServiceImpl;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private HttpSession session;

    @PostMapping("/login")
    public ResponseEntity<ResponseData<AuthResponseDto>> login(@Valid @RequestBody LoginDto loginDto, Errors errors) throws AuthenticationException {
        ResponseData<AuthResponseDto> responseData = new ResponseData<>();
        if (errors.hasErrors()) {
            for (ObjectError error : errors.getAllErrors()) {
                responseData.getMessages().add(error.getDefaultMessage());
            }
            responseData.setMessage(null);
            responseData.setSuccess(false);
            responseData.setList(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        } else {
            responseData.setMessages(null);
            responseData.setMessage("account successfully signed in");
        }
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginDto.getEmail(), loginDto.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = jwtUtilsConfig.generateJwtToken(authentication);
        String refreshToken = jwtUtilsConfig.generateRefresJwtToken(authentication);
        UserDetailsImpl principal = (UserDetailsImpl) authentication.getPrincipal();
        responseData.setSuccess(true);
        responseData.setList(null);
        responseData.setData(new AuthResponseDto(session.getId(), token, refreshToken, principal.getFullName(), principal.getEmail()));
        return ResponseEntity.ok().body(responseData);
    }

    @PostMapping("/register")
    public ResponseEntity<ResponseData<AccountProfileEntities>> register(@Valid @RequestBody RegisterDto regist, Errors errors) {
        ResponseData<AccountProfileEntities> responseData = new ResponseData<>();
        if (errors.hasErrors()) {
            for (ObjectError error : errors.getAllErrors()) {
                responseData.getMessages().add(error.getDefaultMessage());
            }
            responseData.setMessage(null);
            responseData.setSuccess(false);
            responseData.setList(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        } else {
            responseData.setMessages(null);
            responseData.setMessage("account successfully registered");
        }
        regist.setPassword(passwordEncoder.encode(regist.getPassword()));
        AccountProfileEntities profile = modelMapper.map(regist, AccountProfileEntities.class);
        profile.setAccountLoginEntities(modelMapper.map(regist, AccountLoginEntities.class));

        responseData.setSuccess(true);
        responseData.setList(null);
        responseData.setData(accountProfileService.create(profile));
        return ResponseEntity.ok(responseData);
    }

    @PostMapping("/refrest-token")
    public ResponseEntity<AuthResponseDto> refreshToken(@RequestBody RefreshTokenDto refresh) {
        String token = refresh.getRefreshToken();
        boolean valid = jwtUtilsConfig.validateJwtToken(token);
        if (!valid) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        String email = jwtUtilsConfig.getEmailFromJwtToken(token);
        UserDetailsImpl userDetailsImpl = (UserDetailsImpl) userDetailServiceImpl.loadUserByUsername(email);
        Authentication authentication = new UsernamePasswordAuthenticationToken(userDetailsImpl, null, userDetailsImpl.getAuthorities());
        String newToken = jwtUtilsConfig.generateJwtToken(authentication);
        String refreshToken = jwtUtilsConfig.generateJwtToken(authentication);
        return ResponseEntity.ok(new AuthResponseDto(session.getId(), newToken, refreshToken, userDetailsImpl.getFullName(), userDetailsImpl.getEmail()));
    }
}