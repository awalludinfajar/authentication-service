package com.authentication.controllers;

import com.authentication.dto.AccountDeactiveDto;
import com.authentication.dto.ResponseData;
import com.authentication.service.AccountDeactivedService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1/account")
public class DeactivedAccountController {
    
    @Autowired
    private AccountDeactivedService accountDeactivedService;

    // @Autowired
    // private ModelMapper modelMapper;

    @PostMapping("/unregister")
    public ResponseEntity<ResponseData> deactiveAccount(@Valid @RequestBody AccountDeactiveDto accountDeactiveDto, Errors errors) {
        ResponseData responseData = new ResponseData<>();
        if (errors.hasErrors()) {
            for (ObjectError error : errors.getAllErrors()) {
                responseData.getMessages().add(error.getDefaultMessage());
            }
            responseData.setMessage(null);
            responseData.setSuccess(false);
            responseData.setList(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        } else {
            responseData.setMessages(null);
            responseData.setMessage("account successfully unregistered");
        }

        responseData.setData(accountDeactivedService.send(accountDeactiveDto));
        return ResponseEntity.ok(responseData);
    }
}
