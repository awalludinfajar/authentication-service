package com.authentication.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;


@RestController
@RequestMapping("test/sample/url")
public class SampleController {
    
    @GetMapping("/text")
    public String sampleText() {
        System.out.println("hello world!");
        return "Hello World!";
    }
    
}
