package com.authentication.models.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

import com.authentication.models.entities.AccountLoginEntities;

@Repository
public interface AccountLoginRepository extends JpaRepository<AccountLoginEntities, Integer>{

    Boolean existsByEmail(String email);

    Optional<AccountLoginEntities> findByEmail(@Param("email") String email);
}
