package com.authentication.models.repos;

import java.util.Optional;

import com.authentication.models.entities.AccountProfileEntities;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountProfileRepository extends JpaRepository<AccountProfileEntities, Long>{
    
    Boolean existsByEmail(String email);

    Optional<AccountProfileEntities> findByEmail(String email);
}
