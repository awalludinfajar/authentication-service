package com.authentication.models.repos;

import com.authentication.models.entities.AccountDeactivedEntities;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountDeactivedRepository extends JpaRepository<AccountDeactivedEntities, Long> {
    
}
