package com.authentication.models.entities;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Data
@NoArgsConstructor
@Table(name = "P_ACCOUNT_LOGIN")
public class AccountLoginEntities implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @GenericGenerator(
            name = "serviceClassGenerator", parameters = {
            @org.hibernate.annotations.Parameter(name = "sequence_name", value = "SERVICE_CLASS_SEQ"),
            @org.hibernate.annotations.Parameter(name = "increment_size", value = "1"),
            @org.hibernate.annotations.Parameter(name = "initial_value", value = "1"),
    })
    @Id
    @GeneratedValue(generator = "serviceClassGenerator", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID")
    private Long id;

    @OneToOne(mappedBy = "accountLoginEntities")
    private AccountProfileEntities accountProfileEntities;

    @Column(length = 255, nullable = false, name = "FULL_NAME")
    private String fullName;

    @Column(length = 255, nullable = false, name = "EMAIL", unique = true)
    private String email;

    @Column(length = 255, nullable = false, name = "PASSWORD")
    @JsonIgnore
    private String password;

    @Column(nullable = false, name = "CREATED_AT")
    private final Long createdAt = System.currentTimeMillis();
    
    @Column(nullable = false, name = "MODIFIED_AT")
    private final Long modifiedAt = System.currentTimeMillis();

    // public AccountLoginEntities (String email) {
    //     this.email = email;
    // }

    public AccountLoginEntities(String fullName, String email, String password) {
        this.fullName = fullName;
        this.email = email;
        this.password = password;
    }
}
