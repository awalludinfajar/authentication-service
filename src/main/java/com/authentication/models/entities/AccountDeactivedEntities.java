package com.authentication.models.entities;

import java.io.Serializable;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Data
@NoArgsConstructor
@Table(name = "P_ACCOUNT_DEACTIVATED")
public class AccountDeactivedEntities implements Serializable{
    private static final long serialVersionUID = 1L;

    @GenericGenerator(
            name = "serviceClassGenerator", parameters = {
            @org.hibernate.annotations.Parameter(name = "sequence_name", value = "SERVICE_CLASS_SEQ"),
            @org.hibernate.annotations.Parameter(name = "increment_size", value = "1"),
            @org.hibernate.annotations.Parameter(name = "initial_value", value = "1"),
    })
    @Id
    @GeneratedValue(generator = "serviceClassGenerator", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID")
    private Long id;

    @Column(length = 255, nullable = false, name = "ACCOUNT_ID")
    private Long accountId;

    @Column(length = 255, nullable = false, name = "FULL_NAME")
    private String fullName;

    @Column(length = 255, nullable = false, name = "EMAIL", unique = true)
    private String email;

    @Column(length = 255, nullable = false, name = "GENDER")
    private String gender;

    @Column(length = 255, nullable = false, name = "DATE_OF_BIRTH")
    private String dateOfBirth;

    @Column(nullable = false, name = "IDENTITY_TYPE")
    private Integer identityType;

    @Column(length = 255, nullable = false, name = "IDENTITY_NUMBER")
    private String identityNumber;

    @Column(length = 255, nullable = false, name = "ADDRESS")
    private String address;

    @Column(length = 255, nullable = false, name = "PHONE_NUMBER")
    private String phoneNumber;

    @Column(nullable = false, name = "ACCOUNT_TYPE")
    private Integer accountType;

    @Column(nullable = false, name = "DEACTIVATED_AT")
    private final Long deactivatedAt = System.currentTimeMillis();

    @Column(nullable = true, name = "DEACTIVATED_STATUS")
    private Long deactivatedStatus;

    @Column(nullable = false, name = "DEACTIVATED_REASON_ID")
    private Long deactivatedReasonId;

    @Column(nullable = false, name = "DEACTIVATED_DESCR")
    private String deactivatedDescr;

    @Column(nullable = false, name = "CREATED_AT")
    private final Long createdAt = System.currentTimeMillis();
}
