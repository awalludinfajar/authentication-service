package com.authentication.models.entities;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@Table(name = "P_CORPORATE")
public class CorporateEntities implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @GenericGenerator(
            name = "serviceClassGenerator", parameters = {
            @org.hibernate.annotations.Parameter(name = "sequence_name", value = "SERVICE_CLASS_SEQ"),
            @org.hibernate.annotations.Parameter(name = "increment_size", value = "1"),
            @org.hibernate.annotations.Parameter(name = "initial_value", value = "1"),
    })
    @Id
    @GeneratedValue(generator = "serviceClassGenerator", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID")
    private Long id;

    private Long createdAt;
    private Long modifiedAt;
}
