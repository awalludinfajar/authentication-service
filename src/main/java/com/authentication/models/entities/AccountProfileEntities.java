package com.authentication.models.entities;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@Table(name = "P_ACCOUNT_PROFILE")
public class AccountProfileEntities implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @GenericGenerator(
            name = "serviceClassGenerator", parameters = {
            @org.hibernate.annotations.Parameter(name = "sequence_name", value = "SERVICE_CLASS_SEQ"),
            @org.hibernate.annotations.Parameter(name = "increment_size", value = "1"),
            @org.hibernate.annotations.Parameter(name = "initial_value", value = "1"),
    })
    @Id
    @GeneratedValue(generator = "serviceClassGenerator", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID")
    private Long id;

    @Column(length = 255, nullable = false, name = "FULL_NAME")
    private String fullName;

    @Column(length = 255, nullable = false, name = "EMAIL", unique = true)
    private String email;

    @Column(length = 255, nullable = false, name = "GENDER")
    private String gender;

    @Column(length = 255, nullable = false, name = "DATE_OF_BIRTH")
    private String dateOfBirth;

    @Column(nullable = false, name = "IDENTITY_TYPE")
    private Integer identityType;

    @Column(length = 255, nullable = false, name = "IDENTITY_NUMBER")
    private String identityNumber;

    @Column(length = 255, nullable = false, name = "ADDRESS")
    private String address;

    @Column(length = 255, nullable = false, name = "PHONE_NUMBER")
    private String phoneNumber;

    @Column(nullable = false, name = "ACCOUNT_TYPE")
    private Integer accountType;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "ACCOUNT_LOGIN_ID", referencedColumnName = "ID")
    private AccountLoginEntities accountLoginEntities;

    @Column(nullable = false, name = "CREATED_AT")
    private final Long createdAt = System.currentTimeMillis();

    @Column(nullable = false, name = "MODIFIED_AT")
    private final Long modifiedAt = System.currentTimeMillis();

    public AccountProfileEntities(String fullName, String email, String gender, String dateOfBirth,
            Integer identityType, String identityNumber, String address, String phoneNumber, Integer accountType,
            AccountLoginEntities accountLoginEntities) {
        this.fullName = fullName;
        this.email = email;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.identityType = identityType;
        this.identityNumber = identityNumber;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.accountType = accountType;
        this.accountLoginEntities = accountLoginEntities;
    }
    
}