package com.authentication.dto;

import java.io.Serializable;

import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class LoginDto implements Serializable {

    @NotEmpty(message = "Email Is Required")
    private String email;

    @NotEmpty(message = "Password Is Required")
    private String password;
}
