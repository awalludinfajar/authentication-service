package com.authentication.dto;

import jakarta.servlet.http.HttpSession;
import lombok.Data;

@Data
public class AuthResponseDto {
    private String accessToken;
    private String refreshToken;
    private String tokenType = "Bearer ";
    private String fullName;
    private String email;
    private String sessionId;

    public AuthResponseDto(
            String sessionId,
            String accessToken,
            String refreshToken,
            String fullName,
            String email) {
        this.sessionId = sessionId;
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.fullName = fullName;
        this.email = email;
    }
}
