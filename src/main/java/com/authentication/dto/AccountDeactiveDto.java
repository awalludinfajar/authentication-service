package com.authentication.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class AccountDeactiveDto implements Serializable{
    
    private Long accountId;
    private Long deactivatedReasonId;
    private String deactivatedDescr;
}
