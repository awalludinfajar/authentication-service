package com.authentication.dto;

import java.io.Serializable;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class RegisterDto implements Serializable {

    @NotEmpty(message = "Full Name Is Required")
    private String fullName;

    @NotEmpty(message = "Email Is Required")
    @Email
    private String email;

    @NotEmpty(message = "Password Required")
    private String password;

    private String gender;

    private String dateOfBirth;

    // @NotEmpty(message = "Identity Type Is Required")
    private Integer identityType;

    // @NotEmpty(message = "Identity Number Is Required")
    private String identityNumber;

    private String address;

    @NotEmpty(message = "Phone Number Is Required")
    private String phoneNumber;

    private Integer accountType;

    public RegisterDto(String fullName, String email, String password) {
        this.fullName = fullName;
        this.email = email;
        this.password = password;
    }

}
