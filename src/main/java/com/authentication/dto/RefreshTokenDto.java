package com.authentication.dto;

import lombok.Data;

@Data
public class RefreshTokenDto {
    private String refreshToken;
}
