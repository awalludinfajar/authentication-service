package com.authentication.service;

import java.util.List;
import java.util.Optional;

import com.authentication.models.entities.AccountLoginEntities;
import com.authentication.models.repos.AccountLoginRepository;
import com.authentication.utilities.exception.ResourceNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.authentication.utilities.exception.BadRequestException;

@Service
public class AccountLoginService {
    
    @Autowired
    private AccountLoginRepository accountLoginRepository;

    public AccountLoginEntities findById(Integer id) {
        return accountLoginRepository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("Account with id " + id + " Not Found"));
    }

    public AccountLoginEntities findUseEmail(String email) {
        Optional<AccountLoginEntities> temp = accountLoginRepository.findByEmail(email);
        if (!temp.isPresent()) {
            return null;
        }
        return temp.get();
    }
    
    public List<AccountLoginEntities> findAll() {
        return accountLoginRepository.findAll();
    }

    public AccountLoginEntities create(AccountLoginEntities auth) {
        if (!StringUtils.hasText(auth.getFullName())) {
            throw new BadRequestException("Full Name is Required");
        }

        if (!StringUtils.hasText(auth.getEmail())) {
            throw new BadRequestException("Email is Required");
        }

        if (accountLoginRepository.existsByEmail(auth.getEmail())) {
            throw new BadRequestException("Email "+ auth.getEmail() +" Was Registered");
        }

        return accountLoginRepository.save(auth);
    }

    public AccountLoginEntities update(AccountLoginEntities account) {
        return accountLoginRepository.save(account);
    }

    public void deleteById(AccountLoginEntities id) {
        accountLoginRepository.delete(id);
    }
}
