package com.authentication.service;

import com.authentication.dto.AccountDeactiveDto;
import com.authentication.models.entities.AccountDeactivedEntities;
import com.authentication.models.entities.AccountProfileEntities;
import com.authentication.models.repos.AccountDeactivedRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountDeactivedService {

    @Autowired
    private AccountDeactivedRepository accountDeactivedRepository;

    @Autowired
    private AccountProfileService accountProfileService;

    @Autowired
    private ModelMapper modelMapper;
     
    public AccountDeactiveDto send(AccountDeactiveDto account) {

        AccountProfileEntities accountProfileEntities = accountProfileService.findById(account.getAccountId());
        AccountDeactivedEntities accountDeactivedEntities = modelMapper.map(account, AccountDeactivedEntities.class);
        accountDeactivedEntities.setFullName(accountProfileEntities.getFullName());
        accountDeactivedEntities.setEmail(accountProfileEntities.getEmail());
        accountDeactivedEntities.setGender(accountProfileEntities.getGender());
        accountDeactivedEntities.setDateOfBirth(accountProfileEntities.getDateOfBirth());
        accountDeactivedEntities.setIdentityType(accountProfileEntities.getIdentityType());
        accountDeactivedEntities.setIdentityNumber(accountProfileEntities.getIdentityNumber());
        accountDeactivedEntities.setAddress(accountProfileEntities.getAddress());
        accountDeactivedEntities.setPhoneNumber(accountProfileEntities.getPhoneNumber());
        accountDeactivedEntities.setAccountType(accountProfileEntities.getAccountType());
        accountDeactivedRepository.save(accountDeactivedEntities);
        accountProfileService.deleteById(account.getAccountId());
        return account;
    }
}
