package com.authentication.service;

import java.util.Optional;

import com.authentication.models.entities.AccountProfileEntities;
import com.authentication.models.repos.AccountProfileRepository;
import com.authentication.utilities.exception.BadRequestException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class AccountProfileService {
    
    @Autowired
    private AccountProfileRepository accountProfileRepository;

    public AccountProfileEntities create(AccountProfileEntities account) {
        if (!StringUtils.hasText(account.getFullName())) {
            throw new BadRequestException("Full Name is Required");
        }

        if (!StringUtils.hasText(account.getEmail())) {
            throw new BadRequestException("Email is Required");
        }

        if (accountProfileRepository.existsByEmail(account.getEmail())) {
            throw new BadRequestException("Email "+ account.getEmail() +" Was Registered");
        }

        return accountProfileRepository.save(account);
    }

    public AccountProfileEntities findById(Long id) {
        Optional<AccountProfileEntities> temp = accountProfileRepository.findById(id);
        if (!temp.isPresent()) {
            return null;
        }
        return temp.get();
    }

    public AccountProfileEntities findUseEmail(String email) {
        Optional<AccountProfileEntities> temp = accountProfileRepository.findByEmail(email);
        if (!temp.isPresent()) {
            return null;
        }
        AccountProfileEntities res = temp.get();
        AccountProfileEntities accountProfileEntities = new AccountProfileEntities();
        accountProfileEntities.setId(res.getId());
        accountProfileEntities.setFullName(res.getFullName());
        accountProfileEntities.setEmail(res.getEmail());
        accountProfileEntities.setGender(res.getGender());
        accountProfileEntities.setDateOfBirth(res.getDateOfBirth());
        accountProfileEntities.setIdentityType(res.getIdentityType());
        accountProfileEntities.setIdentityNumber(res.getIdentityNumber());
        accountProfileEntities.setAddress(res.getAddress());
        accountProfileEntities.setPhoneNumber(res.getPhoneNumber());
        accountProfileEntities.setAccountType(res.getAccountType());
        return accountProfileEntities;
    }

    // public List<AccountProfileEntities> findAll() {
    //     return accountProfileRepository.findAll();
    // }

    public void deleteById(Long id) {
        accountProfileRepository.deleteById(id);
    }
}
