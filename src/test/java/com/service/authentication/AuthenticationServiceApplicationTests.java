package com.service.authentication;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.authentication.AuthenticationServiceApplication;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import org.junit.jupiter.api.Test;

@SpringBootTest(classes = AuthenticationServiceApplication.class)
class AuthenticationServiceApplicationTests {

	// @Test
	// void contextLoads() {
	// }

	// @Value("${server.port}")
    // private int serverPort;

	// @Test
	// void testServerPort() {
	// 	assertEquals(8082, serverPort, "Server port should be 8082");
	// }
}
