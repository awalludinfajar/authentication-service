# Definition Arguments
FROM maven:3.8.4-openjdk-17 as build

# ENV DB_URL=jdbc:oracle:thin:@swamedia.xyz:1521:ORA
# ENV DB_USERNAME=CORETICKETASDP
# ENV DB_PASSWORD=CORETICKETASDP
# ENV DB_SCHEMA=CORETICKETASDP

RUN mkdir /app
WORKDIR /app

COPY pom.xml .
COPY src ./src

RUN mvn clean install

FROM openjdk:17-alpine

WORKDIR /app

COPY --from=build /app/target/authentication-service-0.0.1-SNAPSHOT.jar authentication-service-0.0.1-SNAPSHOT.jar

EXPOSE 8082

CMD [ "java", "-jar", "authentication-service-0.0.1-SNAPSHOT.jar" ]